#ifndef SAVECONFIGINROOT_H__
#define SAVECONFIGINROOT_H__

//Small helper functions to save the current configuration to a ROOT file
//a) Save current set configuration as string, may be read back to the configuration classes (VirtualConfig)
//b) Save the full status (written and read back values)
//Both functions save a TObjString object with name {name} based on the {config} instance.
//Author: K.Briggl

#include "TObjString.h"
#include "VirtualConfig.h"
#include <sstream>
void SaveConfigInROOT(TVirtualConfig* config, const char* name){
	std::stringstream ss;
	config->Print(true,ss);
	TObjString s(ss.str().c_str());
	s.Write(name);
}
void SaveConfigStatusInROOT(TVirtualConfig* config, const char* name){
	std::stringstream ss;
	config->Print(false,ss);
	TObjString s(ss.str().c_str());
	s.Write(name);
}
#endif
